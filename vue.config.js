const path = require('path');

module.exports = {
  publicPath: './',
  pwa: {
    name: 'Alfalaval',
    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: path.join(process.cwd(), 'src', 'service-worker.js'),
      swDest: path.join(process.cwd(), 'dist', 'service-worker.js'),
      globDirectory: path.join(process.cwd(), 'dist'),
      // ...other Workbox options...
    },
  },
};
