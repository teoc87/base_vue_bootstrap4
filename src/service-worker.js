/* eslint-disable no-restricted-globals */
self.addEventListener('install', (event) => {
  console.log('[Service Worker] Installing Service Worker ...', event);
});

self.addEventListener('activate', (event) => {
  console.log('[Service Worker] Activating Service Worker ...', event);
  return self.clients.claim();
});

self.addEventListener('fetch', (event) => {
  console.log('[Service Worker] Fetching something ....', event);
  event.respondWith(fetch(event.request));
});

/* self.addEventListener('beforeinstallprompt', (event) => {
  event.preventDefault();
  let deferredPrompt = event;

   if (promptButton) {
    if (deferredPrompt) {
      // prompt has been requested
      containerPromptButton.style.display = "inherit";
      promptButton.addEventListener("click", function(e) {
        // delegate the prompt to user action
        e.preventDefault();
        deferredPrompt.prompt();
        deferredPrompt = null;
      });
    }
  }
}); */
